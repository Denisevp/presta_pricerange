```
#!php <?php //AdminSpecificPriceRuleController.php
/**
 * 2007-2016 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author 	PrestaShop SA <contact@prestashop.com>
 *  @copyright  2007-2016 PrestaShop SA
 *  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

/**
 * @property SpecificPriceRule $object
 */
class AdminSpecificPriceRuleControllerCore extends AdminController
{
    public $list_reduction_type;

    public function __construct()
    {
        $this->bootstrap = true;
        $this->table = 'specific_price_rule';
        $this->className = 'SpecificPriceRule';
        $this->lang = false;
        $this->multishop_context = Shop::CONTEXT_ALL;

        /* if $_GET['id_shop'] is transmitted, virtual url can be loaded in config.php, so we wether transmit shop_id in herfs */
        if ($this->id_shop = (int)Tools::getValue('shop_id')) {
            $_GET['id_shop'] = $this->id_shop;
            $_POST['id_shop'] = $this->id_shop;
        }

        $this->list_reduction_type = array(
            'percentage' => $this->l('Percentage'),
            'amount' => $this->l('Amount')
        );

        $this->addRowAction('edit');
        $this->addRowAction('delete');

        $this->context = Context::getContext();

        $this->_select = 's.name shop_name, cu.name currency_name, cl.name country_name, gl.name group_name';
        $this->_join = 'LEFT JOIN '._DB_PREFIX_.'shop s ON (s.id_shop = a.id_shop)
		LEFT JOIN '._DB_PREFIX_.'currency cu ON (cu.id_currency = a.id_currency)
		LEFT JOIN '._DB_PREFIX_.'country_lang cl ON (cl.id_country = a.id_country AND cl.id_lang='.(int)$this->context->language->id.')
		LEFT JOIN '._DB_PREFIX_.'group_lang gl ON (gl.id_group = a.id_group AND gl.id_lang='.(int)$this->context->language->id.')';
        $this->_use_found_rows = false;

        $this->bulk_actions = array(
            'delete' => array(
                'text' => $this->l('Delete selected'),
                'confirm' => $this->l('Delete selected items?'),
                'icon' => 'icon-trash'
            )
        );

        $this->fields_list = array(
            'id_specific_price_rule' => array(
                'title' => $this->l('ID'),
                'align' => 'center',
                'class' => 'fixed-width-xs'
            ),
            'name' => array(
                'title' => $this->l('Name'),
                'filter_key' => 'a!name',
                'width' => 'auto'
            ),
            'shop_name' => array(
                'title' => $this->l('Shop'),
                'filter_key' => 's!name'
            ),
            'currency_name' => array(
                'title' => $this->l('Currency'),
                'align' => 'center',
                'filter_key' => 'cu!name'
            ),
            'country_name' => array(
                'title' => $this->l('Country'),
                'align' => 'center',
                'filter_key' => 'cl!name'
            ),
            'group_name' => array(
                'title' => $this->l('Group'),
                'align' => 'center',
                'filter_key' => 'gl!name'
            ),
            'from_quantity' => array(
                'title' => $this->l('From quantity'),
                'align' => 'center',
                'class' => 'fixed-width-xs'
            ),
            'reduction_type' => array(
                'title' => $this->l('Reduction type'),
                'align' => 'center',
                'type' => 'select',
                'filter_key' => 'a!reduction_type',
                'list' => $this->list_reduction_type,
            ),
            'reduction' => array(
                'title' => $this->l('Reduction'),
                'align' => 'center',
                'type' => 'decimal',
                'class' => 'fixed-width-xs'
            ),
            'from' => array(
                'title' => $this->l('Beginning'),
                'align' => 'right',
                'type' => 'datetime',
            ),
            'to' => array(
                'title' => $this->l('End'),
                'align' => 'right',
                'type' => 'datetime'
            ),
	     //pricerange1 start---
            'Price from' => array(
                'title' => $this->l('Price range from'),
                'align' => 'right',
                'type' => 'decimal',
            ),
            'Price to' => array(
                'title' => $this->l('Price range to'),
                'align' => 'right',
                'type' => 'decimal'
            ),
	     //---pricerange1 end	     
        );

        parent::__construct();
    }

    public function initPageHeaderToolbar()
    {
        if (empty($this->display)) {
            $this->page_header_toolbar_btn['new_specific_price_rule'] = array(
                'href' => self::$currentIndex.'&addspecific_price_rule&token='.$this->token,
                'desc' => $this->l('Add new catalog price rule', null, null, false),
                'icon' => 'process-icon-new'
            );
        }

        parent::initPageHeaderToolbar();
    }

    public function getList($id_lang, $order_by = null, $order_way = null, $start = 0, $limit = null, $id_lang_shop = false)
    {
        parent::getList($id_lang, $order_by, $order_way, $start, $limit, $id_lang_shop);

        foreach ($this->_list as $k => $list) {
            if ($list['reduction_type'] == 'amount') {
                $this->_list[$k]['reduction_type'] = $this->list_reduction_type['amount'];
            } elseif ($list['reduction_type'] == 'percentage') {
                $this->_list[$k]['reduction_type'] = $this->list_reduction_type['percentage'];
            }
        }
    }

    public function renderForm()
    {
        if (!$this->object->id) {
            $this->object->price = -1;
        }

        $this->fields_form = array(
            'legend' => array(
                'title' => $this->l('Catalog price rules'),
                'icon' => 'icon-dollar'
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('Name'),
                    'name' => 'name',
                    'maxlength' => 32,
                    'required' => true,
                    'hint' => $this->l('Forbidden characters').' <>;=#{}'
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Shop'),
                    'name' => 'shop_id',
                    'options' => array(
                        'query' => Shop::getShops(),
                        'id' => 'id_shop',
                        'name' => 'name'
                    ),
                    'condition' => Shop::isFeatureActive(),
                    'default_value' => Shop::getContextShopID()
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Currency'),
                    'name' => 'id_currency',
                    'options' => array(
                        'query' => array_merge(array(0 => array('id_currency' => 0, 'name' => $this->l('All currencies'))), Currency::getCurrencies(false, true, true)),
                        'id' => 'id_currency',
                        'name' => 'name'
                    ),
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Country'),
                    'name' => 'id_country',
                    'options' => array(
                        'query' => array_merge(array(0 => array('id_country' => 0, 'name' => $this->l('All countries'))), Country::getCountries((int)$this->context->language->id)),
                        'id' => 'id_country',
                        'name' => 'name'
                    ),
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Group'),
                    'name' => 'id_group',
                    'options' => array(
                        'query' => array_merge(array(0 => array('id_group' => 0, 'name' => $this->l('All groups'))), Group::getGroups((int)$this->context->language->id)),
                        'id' => 'id_group',
                        'name' => 'name'
                    ),
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('From quantity'),
                    'name' => 'from_quantity',
                    'maxlength' => 10,
                    'required' => true,
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Price (tax excl.)'),
                    'name' => 'price',
                    'disabled' => ($this->object->price == -1 ? 1 : 0),
                    'maxlength' => 10,
                    'suffix' => $this->context->currency->getSign('right'),

                ),
                array(
                    'type' => 'checkbox',
                    'name' => 'leave_bprice',
                    'values' => array(
                        'query' => array(
                            array(
                                'id' => 'on',
                                'name' => $this->l('Leave base price'),
                                'val' => '1',
                                'checked' => '1'
                            ),
                        ),
                        'id' => 'id',
                        'name' => 'name'
                    )
                ),
                array(
                    'type' => 'datetime',
                    'label' => $this->l('From'),
                    'name' => 'from'
                ),
                array(
                    'type' => 'datetime',
                    'label' => $this->l('To'),
                    'name' => 'to'
                ),
//pricerange2 start---		  
                array(
                    'type' => 'text',
                    'label' => $this->l('Price range from'),
                    'name' => 'pricea',
                    'disabled' => ($this->object->pricea == -1 ? 1 : 0),		      
                    'maxlength' => 10,
                    'suffix' => $this->context->currency->getSign('right')	      
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Price range to'),
                    'name' => 'priceb',
                    'disabled' => ($this->object->priceb == -1 ? 1 : 0),		      
                    'maxlength' => 10,
                    'suffix' => $this->context->currency->getSign('right')	      
                ),	
                array(
                    'type' => 'checkbox',
                    'name' => 'no_range',
                    'values' => array(
                        'query' => array(
                            array(
                                'id' => 'on',
                                'name' => $this->l('Do not use a price range'),
                                'val' => '1',
                                'checked' => '1'
                            ),
                        ),
                        'id' => 'id',
                        'name' => 'name'
                    )
                ),	
//---pricerange2 end		  
                array(
                    'type' => 'select',
                    'label' => $this->l('Reduction type'),
                    'name' => 'reduction_type',
                    'options' => array(
                        'query' => array(array('reduction_type' => 'amount', 'name' => $this->l('Amount')), array('reduction_type' => 'percentage', 'name' => $this->l('Percentage'))),
                        'id' => 'reduction_type',
                        'name' => 'name'
                    ),
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Reduction with or without taxes'),
                    'name' => 'reduction_tax',
                    'align' => 'center',
                    'options' => array(
                        'query' => array(
                                        array('lab' => $this->l('Tax included'), 'val' => 1),
                                        array('lab' => $this->l('Tax excluded'), 'val' => 0),
                                    ),
                        'id' => 'val',
                        'name' => 'lab',
                    )
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Reduction'),
                    'name' => 'reduction',
                    'required' => true,
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save')
            ),
        );
        if (($value = $this->getFieldValue($this->object, 'price')) != -1) {
            $price = number_format($value, 6);
        } else {
            $price = '';
        }
//pricerange3 start---	 
        if (($value = $this->getFieldValue($this->object, 'pricea')) != -1) {
            $pricea = number_format($value, 6);
        } else {
            $pricea = '';
        }
        if (($value = $this->getFieldValue($this->object, 'priceb')) != -1) {
            $priceb = number_format($value, 6);
        } else {
            $priceb = '';
        }	
//---pricerange3 end		 
        $this->fields_value = array(
            'price' => $price,
//pricerange4 start---	     
	     'pricea' => $pricea,
	     'priceb' => $priceb,
//---pricerange4 end		     
            'from_quantity' => (($value = $this->getFieldValue($this->object, 'from_quantity')) ? $value : 1),
            'reduction' => number_format((($value = $this->getFieldValue($this->object, 'reduction')) ? $value : 0), 6),
            'leave_bprice_on' => $price ? 0 : 1,
//pricerange5 start---	     
            'leave_pricea_on' => $pricea ? 0 : 1,	     
            'leave_priceb_on' => $priceb ? 0 : 1,	     
//---pricerange5 end		     
            'shop_id' => (($value = $this->getFieldValue($this->object, 'id_shop')) ? $value : 1)
        );

        $attribute_groups = array();
        $attributes = Attribute::getAttributes((int)$this->context->language->id);
        foreach ($attributes as $attribute) {
            if (!isset($attribute_groups[$attribute['id_attribute_group']])) {
                $attribute_groups[$attribute['id_attribute_group']]  = array(
                    'id_attribute_group' => $attribute['id_attribute_group'],
                    'name' => $attribute['attribute_group']
                );
            }
            $attribute_groups[$attribute['id_attribute_group']]['attributes'][] = array(
                'id_attribute' => $attribute['id_attribute'],
                'name' => $attribute['name']
            );
        }
        $features = Feature::getFeatures((int)$this->context->language->id);
        foreach ($features as &$feature) {
            $feature['values'] = FeatureValue::getFeatureValuesWithLang((int)$this->context->language->id, $feature['id_feature'], true);
        }

        $this->tpl_form_vars = array(
            'manufacturers' => Manufacturer::getManufacturers(),
            'suppliers' => Supplier::getSuppliers(),
            'attributes_group' => $attribute_groups,
            'features' => $features,
            'categories' => Category::getSimpleCategories((int)$this->context->language->id),
            'conditions' => $this->object->getConditions(),
            'is_multishop' => Shop::isFeatureActive()
            );
        return parent::renderForm();
    }

    public function processSave()
    {
        $_POST['price'] = Tools::getValue('leave_bprice_on') ? '-1' : Tools::getValue('price');
//pricerange6 start---	 
        $_POST['pricea'] = Tools::getValue('leave_pricea_on') ? '-1' : Tools::getValue('pricea');
        $_POST['priceb'] = Tools::getValue('leave_priceb_on') ? '-1' : Tools::getValue('priceb');	
//---pricerange6 end		 
        if (Validate::isLoadedObject(($object = parent::processSave()))) {
            /** @var SpecificPriceRule $object */
            $object->deleteConditions();
            foreach ($_POST as $key => $values) {
                if (preg_match('/^condition_group_([0-9]+)$/Ui', $key, $condition_group)) {
                    $conditions = array();
                    foreach ($values as $value) {
                        $condition = explode('_', $value);
                        $conditions[] = array('type' => $condition[0], 'value' => $condition[1]);
                    }
                    $object->addConditions($conditions);
                }
            }
            $object->apply();
            return $object;
        }
    }

    public function postProcess()
    {
        Tools::clearSmartyCache();
        return parent::postProcess();
    }
}

```




```
#!php <?php //classes/SpecificPriceRule.php
/*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class SpecificPriceRuleCore extends ObjectModel
{
    public $name;
    public $id_shop;
    public $id_currency;
    public $id_country;
    public $id_group;
    public $from_quantity;
    public $price;
    public $reduction;
    public $reduction_tax;
    public $reduction_type;
    public $from;
    public $to;
//pricerange1 start---   
    public $price_from;
    public $price_to;   
//---pricerange1 end
 
    protected static $rules_application_enable = true;

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'specific_price_rule',
        'primary' => 'id_specific_price_rule',
        'fields' => array(
            'name' =>            array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'required' => true),
            'id_shop' =>        array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
            'id_country' =>    array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
            'id_currency' =>    array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
            'id_group' =>        array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
            'from_quantity' =>    array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => true),
            'price' =>        array('type' => self::TYPE_FLOAT, 'validate' => 'isNegativePrice', 'required' => true),
            'reduction' =>        array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice', 'required' => true),
            'reduction_tax' =>            array('type' => self::TYPE_INT, 'validate' => 'isBool', 'required' => true),
            'reduction_type' => array('type' => self::TYPE_STRING, 'validate' => 'isReductionType', 'required' => true),
            'from' =>            array('type' => self::TYPE_DATE, 'validate' => 'isDateFormat', 'required' => false),
            'to' =>            array('type' => self::TYPE_DATE, 'validate' => 'isDateFormat', 'required' => false),
//pricerange2 start---
            'price_from' =>            array('type' => self::TYPE_FLOAT, 'validate' => 'isNegativePrice', 'required' => true),
            'price_to' =>            array('type' => self::TYPE_FLOAT, 'validate' => 'isNegativePrice', 'required' => true),
//---pricerange2 end	     
        ),
    );

    protected $webserviceParameters = array(
        'objectsNodeName' => 'specific_price_rules',
        'objectNodeName' => 'specific_price_rule',
        'fields' => array(
            'id_shop' =>                array('xlink_resource' => 'shops', 'required' => true),
            'id_country' =>            array('xlink_resource' => 'countries', 'required' => true),
            'id_currency' =>            array('xlink_resource' => 'currencies', 'required' => true),
            'id_group' =>                array('xlink_resource' => 'groups', 'required' => true),
        ),
    );

    public function delete()
    {
        $this->deleteConditions();
        Db::getInstance()->execute('DELETE FROM '._DB_PREFIX_.'specific_price WHERE id_specific_price_rule='.(int)$this->id);
        return parent::delete();
    }

    public function deleteConditions()
    {
        $ids_condition_group = Db::getInstance()->executeS('SELECT id_specific_price_rule_condition_group
																		 FROM '._DB_PREFIX_.'specific_price_rule_condition_group
																		 WHERE id_specific_price_rule='.(int)$this->id);
        if ($ids_condition_group) {
            foreach ($ids_condition_group as $row) {
                Db::getInstance()->delete('specific_price_rule_condition_group', 'id_specific_price_rule_condition_group='.(int)$row['id_specific_price_rule_condition_group']);
                Db::getInstance()->delete('specific_price_rule_condition', 'id_specific_price_rule_condition_group='.(int)$row['id_specific_price_rule_condition_group']);
            }
        }
    }

    public static function disableAnyApplication()
    {
        SpecificPriceRule::$rules_application_enable = false;
    }

    public static function enableAnyApplication()
    {
        SpecificPriceRule::$rules_application_enable = true;
    }

    public function addConditions($conditions)
    {
        if (!is_array($conditions)) {
            return;
        }

        $result = Db::getInstance()->insert('specific_price_rule_condition_group', array(
            'id_specific_price_rule' =>    (int)$this->id
        ));
        if (!$result) {
            return false;
        }
        $id_specific_price_rule_condition_group = (int)Db::getInstance()->Insert_ID();
        foreach ($conditions as $condition) {
            $result = Db::getInstance()->insert('specific_price_rule_condition', array(
                'id_specific_price_rule_condition_group' => (int)$id_specific_price_rule_condition_group,
                'type' => pSQL($condition['type']),
                'value' => (float)$condition['value'],
            ));
            if (!$result) {
                return false;
            }
        }
        return true;
    }

    public function apply($products = false)
    {
        if (!SpecificPriceRule::$rules_application_enable) {
            return;
        }

        $this->resetApplication($products);
        $products = $this->getAffectedProducts($products);
        foreach ($products as $product) {
            SpecificPriceRule::applyRuleToProduct((int)$this->id, (int)$product['id_product'], (int)$product['id_product_attribute']);
        }
    }

    public function resetApplication($products = false)
    {
        $where = '';
        if ($products && count($products)) {
            $where .= ' AND id_product IN ('.implode(', ', array_map('intval', $products)).')';
        }
        return Db::getInstance()->execute('DELETE FROM '._DB_PREFIX_.'specific_price WHERE id_specific_price_rule='.(int)$this->id.$where);
    }

    /**
     * @param array|bool $products
     */
    public static function applyAllRules($products = false)
    {
        if (!SpecificPriceRule::$rules_application_enable) {
            return;
        }

        $rules = new PrestaShopCollection('SpecificPriceRule');
        foreach ($rules as $rule) {
            /** @var SpecificPriceRule $rule */
            $rule->apply($products);
        }
    }

    public function getConditions()
    {
        $conditions = Db::getInstance()->executeS('
			SELECT g.*, c.*
			FROM '._DB_PREFIX_.'specific_price_rule_condition_group g
			LEFT JOIN '._DB_PREFIX_.'specific_price_rule_condition c
				ON (c.id_specific_price_rule_condition_group = g.id_specific_price_rule_condition_group)
			WHERE g.id_specific_price_rule='.(int)$this->id
        );
        $conditions_group = array();
        if ($conditions) {
            foreach ($conditions as &$condition) {
                if ($condition['type'] == 'attribute') {
                    $condition['id_attribute_group'] = Db::getInstance()->getValue('SELECT id_attribute_group
																										FROM '._DB_PREFIX_.'attribute
																										WHERE id_attribute='.(int)$condition['value']);
                } elseif ($condition['type'] == 'feature') {
                    $condition['id_feature'] = Db::getInstance()->getValue('SELECT id_feature
																								FROM '._DB_PREFIX_.'feature_value
																								WHERE id_feature_value='.(int)$condition['value']);
                }
                $conditions_group[(int)$condition['id_specific_price_rule_condition_group']][] = $condition;
            }
        }
        return $conditions_group;
    }

    /**
     * Return the product list affected by this specific rule.
     *
     * @param bool|array $products Products list limitation.
     * @return array Affected products list IDs.
     * @throws PrestaShopDatabaseException
     */
    public function getAffectedProducts($products = false)
    {
        $conditions_group = $this->getConditions();
        $current_shop_id = Context::getContext()->shop->id;

        $result = array();

        if ($conditions_group) {
            foreach ($conditions_group as $id_condition_group => $condition_group) {
                // Base request
                $query = new DbQuery();
                $query->select('p.`id_product`')
                    ->from('product', 'p')
                    ->leftJoin('product_shop', 'ps', 'p.`id_product` = ps.`id_product`')
                    ->where('ps.id_shop = '.(int)$current_shop_id);

                $attributes_join_added = false;

                // Add the conditions
                foreach ($condition_group as $id_condition => $condition) {
                    if ($condition['type'] == 'attribute') {
                        if (!$attributes_join_added) {
                            $query->select('pa.`id_product_attribute`')
                                ->leftJoin('product_attribute', 'pa', 'p.`id_product` = pa.`id_product`')
                                ->join(Shop::addSqlAssociation('product_attribute', 'pa', false));

                            $attributes_join_added = true;
                        }

                        $query->leftJoin('product_attribute_combination', 'pac'.(int)$id_condition, 'pa.`id_product_attribute` = pac'.(int)$id_condition.'.`id_product_attribute`')
                            ->where('pac'.(int)$id_condition.'.`id_attribute` = '.(int)$condition['value']);
                    } elseif ($condition['type'] == 'manufacturer') {
                        $query->where('p.id_manufacturer = '.(int)$condition['value']);
                    } elseif ($condition['type'] == 'category') {
                        $query->leftJoin('category_product', 'cp'.(int)$id_condition, 'p.`id_product` = cp'.(int)$id_condition.'.`id_product`')
                            ->where('cp'.(int)$id_condition.'.id_category = '.(int)$condition['value']);
                    } elseif ($condition['type'] == 'supplier') {
                        $query->where('EXISTS(
							SELECT
								`ps'.(int)$id_condition.'`.`id_product`
							FROM
								`'._DB_PREFIX_.'product_supplier` `ps'.(int)$id_condition.'`
							WHERE
								`p`.`id_product` = `ps'.(int)$id_condition.'`.`id_product`
								AND `ps'.(int)$id_condition.'`.`id_supplier` = '.(int)$condition['value'].'
						)');
                    } elseif ($condition['type'] == 'feature') {
                        $query->leftJoin('feature_product', 'fp'.(int)$id_condition, 'p.`id_product` = fp'.(int)$id_condition.'.`id_product`')
                            ->where('fp'.(int)$id_condition.'.`id_feature_value` = '.(int)$condition['value']);
                    }
                }

                // Products limitation
                if ($products && count($products)) {
                    $query->where('p.`id_product` IN ('.implode(', ', array_map('intval', $products)).')');
                }

                // Force the column id_product_attribute if not requested
                if (!$attributes_join_added) {
                    $query->select('NULL as `id_product_attribute`');
                }

                $result = array_merge($result, Db::getInstance()->executeS($query));
            }
        } else {
            // All products without conditions
            if ($products && count($products)) {
                $query = new DbQuery();
                $query->select('p.`id_product`')
                    ->select('NULL as `id_product_attribute`')
                    ->from('product', 'p')
                    ->leftJoin('product_shop', 'ps', 'p.`id_product` = ps.`id_product`')
                    ->where('ps.id_shop = '.(int)$current_shop_id);
                $query->where('p.`id_product` IN ('.implode(', ', array_map('intval', $products)).')');
                $result = Db::getInstance()->executeS($query);
            } else {
                $result = array(array('id_product' => 0, 'id_product_attribute' => null));
            }

        }

        return $result;
    }

    public static function applyRuleToProduct($id_rule, $id_product, $id_product_attribute = null)
    {
        $rule = new SpecificPriceRule((int)$id_rule);
        if (!Validate::isLoadedObject($rule) || !Validate::isUnsignedInt($id_product)) {
            return false;
        }

        $specific_price = new SpecificPrice();
        $specific_price->id_specific_price_rule = (int)$rule->id;
        $specific_price->id_product = (int)$id_product;
        $specific_price->id_product_attribute = (int)$id_product_attribute;
        $specific_price->id_customer = 0;
        $specific_price->id_shop = (int)$rule->id_shop;
        $specific_price->id_country = (int)$rule->id_country;
        $specific_price->id_currency = (int)$rule->id_currency;
        $specific_price->id_group = (int)$rule->id_group;
        $specific_price->from_quantity = (int)$rule->from_quantity;
        $specific_price->price = (float)$rule->price;
        $specific_price->reduction_type = $rule->reduction_type;
        $specific_price->reduction_tax = $rule->reduction_tax;
        $specific_price->reduction = ($rule->reduction_type == 'percentage' ? $rule->reduction / 100 : (float)$rule->reduction);
        $specific_price->from = $rule->from;
        $specific_price->to = $rule->to;
//pricerange3 start---
        $specific_price->price_from = $rule->price_from;
        $specific_price->price_to = $rule->price_to;	 
//---pricerange3 end	 
        return $specific_price->add();
    }
}


```







```
#!php <?php //classes/SpecificPrice.php
/*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class SpecificPriceCore extends ObjectModel
{
    public $id_product;
    public $id_specific_price_rule = 0;
    public $id_cart = 0;
    public $id_product_attribute;
    public $id_shop;
    public $id_shop_group;
    public $id_currency;
    public $id_country;
    public $id_group;
    public $id_customer;
    public $price;
    public $from_quantity;
    public $reduction;
    public $reduction_tax = 1;
    public $reduction_type;
    public $from;
    public $to;
//pricerange1 start---     
    public $price_from;
    public $price_to;
 //---pricerange1 end   
    
    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'specific_price',
        'primary' => 'id_specific_price',
        'fields' => array(
            'id_shop_group' =>            array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'id_shop' =>                array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
            'id_cart' =>            array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
            'id_product' =>            array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
            'id_product_attribute' =>    array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'id_currency' =>            array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
            'id_specific_price_rule' =>    array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'id_country' =>            array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
            'id_group' =>                array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
            'id_customer' =>            array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
            'price' =>                    array('type' => self::TYPE_FLOAT, 'validate' => 'isNegativePrice', 'required' => true),
            'from_quantity' =>            array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => true),
            'reduction' =>                array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice', 'required' => true),
            'reduction_tax' =>            array('type' => self::TYPE_INT, 'validate' => 'isBool', 'required' => true),
            'reduction_type' =>        array('type' => self::TYPE_STRING, 'validate' => 'isReductionType', 'required' => true),
            'from' =>                    array('type' => self::TYPE_DATE, 'validate' => 'isDateFormat', 'required' => true),
            'to' =>                    array('type' => self::TYPE_DATE, 'validate' => 'isDateFormat', 'required' => true),
//pricerange1 start--- 
            'price_from' =>                    array('type' => self::TYPE_FLOAT, 'validate' => 'isNegativePrice', 'required' => true),
            'price_to' =>                    array('type' => self::TYPE_FLOAT, 'validate' => 'isNegativePrice', 'required' => true),
//---pricerange1 end	     
        ),
    );

    protected $webserviceParameters = array(
        'objectsNodeName' => 'specific_prices',
        'objectNodeName' => 'specific_price',
            'fields' => array(
            'id_shop_group' =>            array('xlink_resource' => 'shop_groups'),
            'id_shop' =>                array('xlink_resource' => 'shops', 'required' => true),
            'id_cart' =>                array('xlink_resource' => 'carts', 'required' => true),
            'id_product' =>            array('xlink_resource' => 'products', 'required' => true),
            'id_product_attribute' =>        array('xlink_resource' => 'product_attributes'),
            'id_currency' =>            array('xlink_resource' => 'currencies', 'required' => true),
            'id_country' =>            array('xlink_resource' => 'countries', 'required' => true),
            'id_group' =>                array('xlink_resource' => 'groups', 'required' => true),
            'id_customer' =>            array('xlink_resource' => 'customers', 'required' => true),
            ),
    );


    protected static $_specificPriceCache = array();
    protected static $_filterOutCache = array();
    protected static $_cache_priorities = array();
    protected static $_no_specific_values = array();

    /**
     * Flush local cache
     */
    protected function flushCache() {
        SpecificPrice::$_specificPriceCache = array();
        SpecificPrice::$_filterOutCache = array();
        SpecificPrice::$_cache_priorities = array();
        SpecificPrice::$_no_specific_values = array();
        Product::flushPriceCache();
    }

    public function add($autodate = true, $nullValues = false)
    {
        if (parent::add($autodate, $nullValues)) {
            // Flush cache when we adding a new specific price
            $this->flushCache();
            // Set cache of feature detachable to true
            Configuration::updateGlobalValue('PS_SPECIFIC_PRICE_FEATURE_ACTIVE', '1');
            return true;
        }
        return false;
    }

    public function update($null_values = false)
    {
        if (parent::update($null_values)) {
            // Flush cache when we updating a new specific price
            $this->flushCache();
            return true;
        }
        return false;
    }

    public function delete()
    {
        if (parent::delete()) {
            // Flush cache when we deletind a new specific price
            $this->flushCache();
            // Refresh cache of feature detachable
            Configuration::updateGlobalValue('PS_SPECIFIC_PRICE_FEATURE_ACTIVE', SpecificPrice::isCurrentlyUsed($this->def['table']));
            return true;
        }
        return false;
    }

    public static function getByProductId($id_product, $id_product_attribute = false, $id_cart = false)
    {
        return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
			SELECT *
			FROM `'._DB_PREFIX_.'specific_price`
			WHERE `id_product` = '.(int)$id_product.
            ($id_product_attribute ? ' AND id_product_attribute = '.(int)$id_product_attribute : '').'
			AND id_cart = '.(int)$id_cart);
    }

    public static function deleteByIdCart($id_cart, $id_product = false, $id_product_attribute = false)
    {
        return Db::getInstance()->execute('
			DELETE FROM `'._DB_PREFIX_.'specific_price`
			WHERE id_cart='.(int)$id_cart.
            ($id_product ? ' AND id_product='.(int)$id_product.' AND id_product_attribute='.(int)$id_product_attribute : ''));
    }

    public static function getIdsByProductId($id_product, $id_product_attribute = false, $id_cart = 0)
    {
        return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
			SELECT `id_specific_price`
			FROM `'._DB_PREFIX_.'specific_price`
			WHERE `id_product` = '.(int)$id_product.'
			AND id_product_attribute='.(int)$id_product_attribute.'
			AND id_cart='.(int)$id_cart);
    }

    /**
     * score generation for quantity discount
     */
    protected static function _getScoreQuery($id_product, $id_shop, $id_currency, $id_country, $id_group, $id_customer)
    {
        $select = '(';

        $priority = SpecificPrice::getPriority($id_product);
        foreach (array_reverse($priority) as $k => $field) {
            if (!empty($field)) {
                $select .= ' IF (`'.bqSQL($field).'` = '.(int)$$field.', '.pow(2, $k + 1).', 0) + ';
            }
        }

        return rtrim($select, ' +').') AS `score`';
    }

    public static function getPriority($id_product)
    {
        if (!SpecificPrice::isFeatureActive()) {
            return explode(';', Configuration::get('PS_SPECIFIC_PRICE_PRIORITIES'));
        }

        if (!isset(SpecificPrice::$_cache_priorities[(int)$id_product])) {
            SpecificPrice::$_cache_priorities[(int)$id_product] = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('
				SELECT `priority`, `id_specific_price_priority`
				FROM `'._DB_PREFIX_.'specific_price_priority`
				WHERE `id_product` = '.(int)$id_product.'
				ORDER BY `id_specific_price_priority` DESC
			');
        }

        $priority = SpecificPrice::$_cache_priorities[(int)$id_product];

        if (!$priority) {
            $priority = Configuration::get('PS_SPECIFIC_PRICE_PRIORITIES');
        }
        $priority = 'id_customer;'.$priority;

        return preg_split('/;/', $priority);
    }

    /**
     * Remove or add a field value to a query if values are present in the database (cache friendly)
     *
     * @param string $field_name
     * @param int $field_value
     * @param int $threshold
     * @return string
     * @throws PrestaShopDatabaseException
     */
    protected static function filterOutField($field_name, $field_value, $threshold = 1000)
    {
        $query_extra = 'AND `'.$field_name.'` = 0 ';
        if ($field_value == 0 || array_key_exists($field_name, self::$_no_specific_values)) {
            return $query_extra;
        }
        $key_cache     = __FUNCTION__.'-'.$field_name.'-'.$threshold;
        $specific_list = array();
        if (!array_key_exists($key_cache, SpecificPrice::$_filterOutCache)) {
            $query_count    = 'SELECT COUNT(DISTINCT `'.$field_name.'`) FROM `'._DB_PREFIX_.'specific_price` WHERE `'.$field_name.'` != 0';
            $specific_count = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($query_count);
            if ($specific_count == 0) {
                self::$_no_specific_values[$field_name] = true;

                return $query_extra;
            }
            if ($specific_count < $threshold) {
                $query             = 'SELECT DISTINCT `'.$field_name.'` FROM `'._DB_PREFIX_.'specific_price` WHERE `'.$field_name.'` != 0';
                $tmp_specific_list = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($query);
                foreach ($tmp_specific_list as $key => $value) {
                    $specific_list[] = $value[$field_name];
                }
            }
            SpecificPrice::$_filterOutCache[$key_cache] = $specific_list;
        } else {
            $specific_list = SpecificPrice::$_filterOutCache[$key_cache];
        }

        // $specific_list is empty if the threshold is reached
        if (empty($specific_list) || in_array($field_value, $specific_list)) {
            $query_extra = 'AND `'.$field_name.'` '.self::formatIntInQuery(0, $field_value).' ';
        }

        return $query_extra;
    }

    /**
     * Remove or add useless fields value depending on the values in the database (cache friendly)
     *
     * @param int|null $id_product
     * @param int|null $id_product_attribute
     * @param int|null $id_cart
     * @param string|null $beginning
     * @param string|null $ending
     * @return string
     */
    protected static function computeExtraConditions($id_product, $id_product_attribute, $id_customer, $id_cart, $beginning = null, $ending = null)
    {
        $first_date = date('Y-m-d 00:00:00');
        $last_date = date('Y-m-d 23:59:59');
        $now = date('Y-m-d H:i:00');
        if ($beginning === null) {
            $beginning = $now;
        }
        if ($ending === null) {
            $ending = $now;
        }
        $id_customer = (int)$id_customer;

        $query_extra = '';

        if ($id_product !== null) {
            $query_extra .= self::filterOutField('id_product', $id_product);
        }

        if ($id_customer !== null) {
            $query_extra .= self::filterOutField('id_customer', $id_customer);
        }

        if ($id_product_attribute !== null) {
            $query_extra .= self::filterOutField('id_product_attribute', $id_product_attribute);
        }

        if ($id_cart !== null) {
            $query_extra .= self::filterOutField('id_cart', $id_cart);
        }

        if ($ending == $now && $beginning == $now) {
            $key = __FUNCTION__.'-'.$first_date.'-'.$last_date;
            if (!array_key_exists($key, SpecificPrice::$_filterOutCache)) {
                $query_from_count    = 'SELECT 1 FROM `'._DB_PREFIX_.'specific_price` WHERE `from` BETWEEN \''.$first_date.'\' AND \''.$last_date.'\'';
                $from_specific_count = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($query_from_count);

                $query_to_count                       = 'SELECT 1 FROM `'._DB_PREFIX_.'specific_price` WHERE `to` BETWEEN \''.$first_date.'\' AND \''.$last_date.'\'';

                $to_specific_count                    = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($query_to_count);
                SpecificPrice::$_filterOutCache[$key] = array($from_specific_count, $to_specific_count);
            } else {
                list($from_specific_count, $to_specific_count) = SpecificPrice::$_filterOutCache[$key];
            }
        } else {
            $from_specific_count = $to_specific_count = 1;
        }

        // if the from and to is not reached during the current day, just change $ending & $beginning to any date of the day to improve the cache
        if (!$from_specific_count && !$to_specific_count) {
            $ending = $beginning = $first_date;
        }

        $query_extra .= ' AND (`from` = \'0000-00-00 00:00:00\' OR \''.$beginning.'\' >= `from`)'
                       .' AND (`to` = \'0000-00-00 00:00:00\' OR \''.$ending.'\' <= `to`)';

        return $query_extra;
    }

    private static function formatIntInQuery($first_value, $second_value) {
        $first_value = (int)$first_value;
        $second_value = (int)$second_value;
        if ($first_value != $second_value) {
            return 'IN ('.$first_value.', '.$second_value.')';
        } else {
            return ' = '.$first_value;
        }
    }

    public static function getSpecificPrice($id_product, $id_shop, $id_currency, $id_country, $id_group, $quantity, $id_product_attribute = null, $id_customer = 0, $id_cart = 0, $real_quantity = 0)
    {
        if (!SpecificPrice::isFeatureActive()) {
            return array();
        }
        /*
        ** The date is not taken into account for the cache, but this is for the better because it keeps the consistency for the whole script.
        ** The price must not change between the top and the bottom of the page
        */

        $key = ((int)$id_product.'-'.(int)$id_shop.'-'.(int)$id_currency.'-'.(int)$id_country.'-'.(int)$id_group.'-'.(int)$quantity.'-'.(int)$id_product_attribute.'-'.(int)$id_cart.'-'.(int)$id_customer.'-'.(int)$real_quantity);
        if (!array_key_exists($key, SpecificPrice::$_specificPriceCache)) {
            $query_extra = self::computeExtraConditions($id_product, $id_product_attribute, $id_customer, $id_cart);
            $query = '
			SELECT *, '.SpecificPrice::_getScoreQuery($id_product, $id_shop, $id_currency, $id_country, $id_group, $id_customer).'
				FROM `'._DB_PREFIX_.'specific_price`
				WHERE
                `id_shop` '.self::formatIntInQuery(0, $id_shop).' AND
                `id_currency` '.self::formatIntInQuery(0, $id_currency).' AND
                `id_country` '.self::formatIntInQuery(0, $id_country).' AND
                `id_group` '.self::formatIntInQuery(0, $id_group).' '.$query_extra.'
				AND IF(`from_quantity` > 1, `from_quantity`, 0) <= ';

            $query .= (Configuration::get('PS_QTY_DISCOUNT_ON_COMBINATION') || !$id_cart || !$real_quantity) ? (int)$quantity : max(1, (int)$real_quantity);
            $query .= ' ORDER BY `id_product_attribute` DESC, `from_quantity` DESC, `id_specific_price_rule` ASC, `score` DESC, `to` DESC, `from` DESC';

            SpecificPrice::$_specificPriceCache[$key] = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($query);
        }
        return SpecificPrice::$_specificPriceCache[$key];
    }

    public static function setPriorities($priorities)
    {
        $value = '';
        if (is_array($priorities)) {
            foreach ($priorities as $priority) {
                $value .= pSQL($priority).';';
            }
        }

        SpecificPrice::deletePriorities();

        return Configuration::updateValue('PS_SPECIFIC_PRICE_PRIORITIES', rtrim($value, ';'));
    }

    public static function deletePriorities()
    {
        return Db::getInstance()->execute('
		TRUNCATE `'._DB_PREFIX_.'specific_price_priority`
		');
    }

    public static function setSpecificPriority($id_product, $priorities)
    {
        $value = '';
        foreach ($priorities as $priority) {
            $value .= pSQL($priority).';';
        }

        return Db::getInstance()->execute('
		INSERT INTO `'._DB_PREFIX_.'specific_price_priority` (`id_product`, `priority`)
		VALUES ('.(int)$id_product.',\''.pSQL(rtrim($value, ';')).'\')
		ON DUPLICATE KEY UPDATE `priority` = \''.pSQL(rtrim($value, ';')).'\'
		');
    }

    public static function getQuantityDiscounts($id_product, $id_shop, $id_currency, $id_country, $id_group, $id_product_attribute = null, $all_combinations = false, $id_customer = 0)
    {
        if (!SpecificPrice::isFeatureActive()) {
            return array();
        }

        $query_extra = self::computeExtraConditions($id_product, ((!$all_combinations)?$id_product_attribute:null), $id_customer, null);
        $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
			SELECT *,
					'.SpecificPrice::_getScoreQuery($id_product, $id_shop, $id_currency, $id_country, $id_group, $id_customer).'
				FROM `'._DB_PREFIX_.'specific_price`
				WHERE
					`id_shop` '.self::formatIntInQuery(0, $id_shop).' AND
					`id_currency` '.self::formatIntInQuery(0, $id_currency).' AND
					`id_country` '.self::formatIntInQuery(0, $id_country).' AND
					`id_group` '.self::formatIntInQuery(0, $id_group).' '.$query_extra.'
					ORDER BY `from_quantity` ASC, `id_specific_price_rule` ASC, `score` DESC, `to` DESC, `from` DESC
		', false, false);

        $targeted_prices = array();
        $last_quantity = array();

        while ($specific_price = Db::getInstance()->nextRow($result)) {
            if (!isset($last_quantity[(int)$specific_price['id_product_attribute']])) {
                $last_quantity[(int)$specific_price['id_product_attribute']] = $specific_price['from_quantity'];
            } elseif ($last_quantity[(int)$specific_price['id_product_attribute']] == $specific_price['from_quantity']) {
                continue;
            }

            $last_quantity[(int)$specific_price['id_product_attribute']] = $specific_price['from_quantity'];
            if ($specific_price['from_quantity'] > 1) {
                $targeted_prices[] = $specific_price;
            }
        }

        return $targeted_prices;
    }

    public static function getQuantityDiscount($id_product, $id_shop, $id_currency, $id_country, $id_group, $quantity, $id_product_attribute = null, $id_customer = 0)
    {
        if (!SpecificPrice::isFeatureActive()) {
            return array();
        }



        $query_extra = self::computeExtraConditions($id_product, $id_product_attribute, $id_customer, null);
        return Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow('
			SELECT *,
					'.SpecificPrice::_getScoreQuery($id_product, $id_shop, $id_currency, $id_country, $id_group, $id_customer).'
			FROM `'._DB_PREFIX_.'specific_price`
			WHERE
					`id_shop` '.self::formatIntInQuery(0, $id_shop).' AND
					`id_currency` '.self::formatIntInQuery(0, $id_currency).' AND
					`id_country` '.self::formatIntInQuery(0, $id_country).' AND
					`id_group` '.self::formatIntInQuery(0, $id_group).' AND
					`from_quantity` >= '.(int)$quantity.' '.$query_extra.'
					ORDER BY `from_quantity` DESC, `score` DESC, `to` DESC, `from` DESC
		');
    }

    public static function getProductIdByDate($id_shop, $id_currency, $id_country, $id_group, $beginning, $ending, $id_customer = 0, $with_combination_id = false)
    {
        if (!SpecificPrice::isFeatureActive()) {
            return array();
        }

        $query_extra = self::computeExtraConditions(null, null, $id_customer, null, $beginning, $ending);
        $results = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
			SELECT `id_product`, `id_product_attribute`
			FROM `'._DB_PREFIX_.'specific_price`
			WHERE	`id_shop` '.self::formatIntInQuery(0, $id_shop).' AND
					`id_currency` '.self::formatIntInQuery(0, $id_currency).' AND
					`id_country` '.self::formatIntInQuery(0, $id_country).' AND
					`id_group` '.self::formatIntInQuery(0, $id_group).' AND
					`from_quantity` = 1 AND
					`reduction` > 0
		'.$query_extra);
        $ids_product = array();
        foreach($results as $key => $value) {
            $ids_product[] = $with_combination_id ? array('id_product' => (int)$value['id_product'], 'id_product_attribute' => (int)$value['id_product_attribute']) : (int)$value['id_product'];
        }

        return $ids_product;
    }

    public static function deleteByProductId($id_product)
    {
        if (Db::getInstance()->execute('DELETE FROM `'._DB_PREFIX_.'specific_price` WHERE `id_product` = '.(int)$id_product)) {
            // Refresh cache of feature detachable
            Configuration::updateGlobalValue('PS_SPECIFIC_PRICE_FEATURE_ACTIVE', SpecificPrice::isCurrentlyUsed('specific_price'));
            return true;
        }
        return false;
    }

    public function duplicate($id_product = false)
    {
        if ($id_product) {
            $this->id_product = (int)$id_product;
        }
        unset($this->id);
        return $this->add();
    }

    /**
     * This method is allow to know if a feature is used or active
     * @since 1.5.0.1
     * @return bool
     */
    public static function isFeatureActive()
    {
        static $feature_active = null;

        if ($feature_active === null) {
            $feature_active = Configuration::get('PS_SPECIFIC_PRICE_FEATURE_ACTIVE');
        }
        return $feature_active;
    }

    public static function exists($id_product, $id_product_attribute, $id_shop, $id_group, $id_country, $id_currency, $id_customer, $from_quantity, $from, $to, $rule = false)
    {
        $rule = ' AND `id_specific_price_rule`'.(!$rule ? '=0' : '!=0');
        return (int)Db::getInstance()->getValue('SELECT `id_specific_price`
												FROM '._DB_PREFIX_.'specific_price
												WHERE `id_product`='.(int)$id_product.' AND
													`id_product_attribute`='.(int)$id_product_attribute.' AND
													`id_shop`='.(int)$id_shop.' AND
													`id_group`='.(int)$id_group.' AND
													`id_country`='.(int)$id_country.' AND
													`id_currency`='.(int)$id_currency.' AND
													`id_customer`='.(int)$id_customer.' AND
													`from_quantity`='.(int)$from_quantity.' AND
													`from` >= \''.pSQL($from).'\' AND
													`to` <= \''.pSQL($to).'\''.$rule);
    }
}


```